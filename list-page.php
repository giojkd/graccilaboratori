<?php /* Template Name: List Page */ ?>
<?php $tp = get_template_directory_uri(); ?>
<?php get_header();?>
<?php
$title = trim(get_the_title());
#$title = str_replace(' ','<br>',$title);
$files = [];
for($i = 1; $i <= 5; $i++){
  $files[] = array_filter((array)get_field('file_'.$i));
}
$files = array_filter($files);

$listPageTitle = (strpos(strtolower($title), 'laboratorio') !== false) ? 'Laboratori' : 'Consulenza'
?>

<style media="screen">
.list-page h1, .list-page h2, .list-page h3{
  color:<?php the_field('color')?>;
}
.list-page ul li:before{
  color:<?php the_field('color')?>;
}
.file-box{
  border: 1px solid <?php the_field('color')?>;
}

.file-box:hover{
  color:<?php the_field('color')?>;
}
</style>
<?php $icon = (array)get_field('icon');?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12  pl-0 pr-0">
      <?php the_post_thumbnail(); ?>
    </div>
  </div>
</div>

<div class="list-page">
  <div class="container-fluid" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
          <span><?=$listPageTitle?></span>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-7 col-md-3 ">
          <h1><?=$title ?></h1>
        </div>
        <div class="col-5 col-md-6">
          <img class="list-page-icon" src="<?=$icon['url']?>" alt="">
        </div>
      </div>
      <div class="row">

        <div class="col-md-6">




          <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
          the_content();
        endwhile; else: ?>
        <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
      <?php
      if(count($files)>0){
        foreach($files as $file){
          ?>
          <a  target="_blank" href="<?=$file['url']?>" class="file-box">
            <img src="<?=$tp?>/css/images/icon-pdf.png" alt=""> <?=$file['title']?>
          </a>
          <?php
        }
      }?>
    </div>
    <div class="col-md-4">
      <img class="scientist" src="<?=$tp?>/css/images/scientist.png" >
    </div>
  </div>
</div>
</div>
</div>

<?php get_footer();?>
