<?php /* Template Name: Consulency */ ?>
<?php $tp = get_template_directory_uri(); ?>
<?php get_header();?>

<div class="container-fluid" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
        <span>I nostri contatti</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 offset-md-3 text-center">
      <div class="contacts" >
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="contacts-icon">
              <i class="fas fa-map-marker"></i>
            </div>
            <h3>Vieni a trovarci</h3>
            <p>Via Volontari della Libertà 29 – Empoli</p>
          </div>
          <div class="col-md-4 text-center">
            <div class="contacts-icon">
              <i class="fas fa-mobile-alt"></i>
            </div>
            <h3>Chiamaci</h3>
            <p>
              Tel (+39) 0571  591184<br>Tel (+39) 0571  591194<br> Fax (+39) 0571 993241
            </p>
          </div>
          <div class="col-md-4 text-center">
            <div class="contacts-icon">
              <i class="far fa-envelope"></i>
            </div>
            <h3>Scrivici</h3>
            <p>
              info@graccilaboratori.it<br>
              formazione@graccilaboratori.it<br>
              ambienteesicurezza@graccilaboratori.it<br>
              alimenti@graccilaboratori.it<br>
              amministrazione@graccilaboratori.it<br><br>
              PEC   patrizio.gracci@pec.chimici.it</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg-revert.png'); ">
          <span style="font-size:25px;">Richiedi un preventivo</span>
        </div>
        <div class="row">
          <div class="col-md-4 offset-md-4">
            <form class="contacts-form" action="index.html" method="post">
              <input type="text" name="" value="" placeholder="Nome" >
              <input type="text" name="" value="" placeholder="Mail" >
              <input type="text" name="" value="" placeholder="Telefono" >
              <textarea name="name" rows="8" placeholder="Messaggio"></textarea>
              <p>
                Questo modulo di contatto raccoglie il tuo nome, l'e-mail e il tuo contenuto.<br>
Per maggiori informazioni consulta la nostra pagina di <a href="/privacy/">Privacy Policy</a><br>
<label><input type="checkbox" required name="" value=""> Ho letto l'informativa privacy e accetto la memorizzazione dei miei dati</label><br>
<label><input type="checkbox" name="" value=""> Acconsento ai sensi del Regolamento Europeo 2016/679 ad essere informato su promozioni e novità tramite email/SMS, a ricevere la vostra newsletter e i questionari per richiedere una prima consulenza gratuita. (Opzionale)</label><br>
              </p>
              <button type="submit" name="button">Invia messaggio</button>
              <img style="z-index:9999" class="scientist d-none d-md-block" src="<?=$tp?>/css/images/scientist.png" alt="">
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row no-gutters">
      <div class="col-md-12">
        <iframe style="margin-bottom:10px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2884.4452194271694!2d10.900582315727663!3d43.701295857336916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a68c5943c0d2f%3A0x57345acd3bc14ef3!2sVia+Volontari+della+Liberta&#39;%2C+29%2C+50053+Empoli+FI!5e0!3m2!1sit!2sit!4v1562613688485!5m2!1sit!2sit" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <?php get_footer();?>
