<?php
require_once('wp_bootstrap_navwalker.php');

function load_stylesheets(){
  wp_register_style('bootstrap', get_template_directory_uri().'/css/bootstrap/css/bootstrap.min.css',[],false,'all');
  wp_register_style('style', get_template_directory_uri().'/style.css',[],false,'all');
  wp_register_style('twentytwenty', get_template_directory_uri().'/css/twentytwenty.css',[],false,'all');
  wp_enqueue_style('bootstrap');
  wp_enqueue_style('style');
  wp_enqueue_style('twentytwenty');

  if(is_page()){ //Check if we are viewing a page
      global $wp_query;
      $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
      if($template_name == 'gallery-page.php'){
        wp_register_style('flickity', get_template_directory_uri().'/css/flickity.css',[],false,'all');
        $template_name = wp_enqueue_style('flickity');
      }
    }
}

function load_js(){

  wp_deregister_script('jquery');
  wp_register_script('jquery',get_template_directory_uri().'/js/jquery-3.4.1.min.js','',1,true);
  wp_enqueue_script('jquery');
  wp_register_script('bootstrap',get_template_directory_uri().'/js/bootstrap.js','',1,true);
  wp_enqueue_script('bootstrap');
  wp_register_script('scrollify',get_template_directory_uri().'/js/scrollify.min.js','',1,true);
  wp_enqueue_script('scrollify');
  wp_register_script('autohidenavbar',get_template_directory_uri().'/js/autohidenavbar.min.js','',1,true);
  wp_enqueue_script('autohidenavbar');
  wp_register_script('event_move',get_template_directory_uri().'/js/jquery.event.move.js','',1,true);
  wp_enqueue_script('event_move');
  wp_register_script('twentytwenty',get_template_directory_uri().'/js/jquery.twentytwenty.js','',1,true);
  wp_enqueue_script('twentytwenty');
  wp_register_script('customjs',get_template_directory_uri().'/js/scripts.js','',1,true);
  wp_enqueue_script('customjs');

  if(is_page()){ //Check if we are viewing a page
      global $wp_query;
      $template_name = get_post_meta( $wp_query->post->ID, '_wp_page_template', true );
      if($template_name == 'catalogue-page.php'){
        wp_register_script('vue',get_template_directory_uri().'/js/vue.js','',1,true);
        wp_register_script('axios',get_template_directory_uri().'/js/axios.js','',1,true);
        wp_register_script('select2',get_template_directory_uri().'/js/select2.js','',1,true);
        wp_register_script('catalogue',get_template_directory_uri().'/js/catalogue.js','',1,true);
        wp_enqueue_script('vue');
        wp_enqueue_script('axios');
        wp_enqueue_script('select2');
        wp_enqueue_script('catalogue');
      }
      if($template_name == 'gallery-page.php'){
        wp_register_script('flickity',get_template_directory_uri().'/js/flickity.js','',1,true);
        wp_enqueue_script('flickity');
        wp_register_script('gallery',get_template_directory_uri().'/js/gallery.js','',1,true);
        wp_enqueue_script('gallery');
      }
   }

}

add_action('wp_enqueue_scripts','load_stylesheets');
add_action('wp_enqueue_scripts','load_js');

add_theme_support('menus');

register_nav_menus([
  'top-menu' => __('Top menu','theme'),
  'footer-menu' => __('Footer menu','theme'),
  'second-footer-menu' => __('Second footer menu','theme'),
]);

add_theme_support( 'post-thumbnails' );


?>
