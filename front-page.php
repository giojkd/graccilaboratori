<?php
get_header();
$tp = get_template_directory_uri();
$classes = [
  [
    'classes' => 'antincendio-rischio-medio',
    'position' => ['top'=>160,'left'=>320],
    'title' => 'Inizio 25 giugno',
    'content' => 'Antincendio<br>rischio medio<br>(8 ore)',
    'color' => '#87c12b'
  ],
  [
    'classes' => 'aggiornamento-antincendio-rischio-medio',
    'position' => ['top'=>230,'left'=>545],
    'title' =>  '27 giugno',
    'content' => 'Aggiornamento<br>Antincendio<br>Rischio Medio<br>(4 ore)',
    'color' => '#e52c47'
  ],
  [
    'classes' => 'antincendio-rischio-basso',
    'position' => ['top'=>490,'left'=>610],
    'title' => '27 giugno',
    'content' => 'Antincendio<br>Rischio Basso<br>(4 ore)',
    'color' => '#3dbea0'
  ],
  [
    'classes' => 'aggiornamento-antincendio-rischio-basso',
    'position' => ['top'=>730,'left'=>540],
    'title' => 'Inizio 25 giugno',
    'content' => 'Antincendio<br>Rischio Medio<br>(8 ore)',
    'color' => '#e06410'
  ],
  [
    'classes' => 'rappresentante-dei-lavori-per-la-sicurezza',
    'position' => ['top'=>810,'left'=>320],
    'title' => 'Inizio 25 giugno',
    'content' => 'Rappresentante dei lavori per la sicurezza (RLS)<br>(32 ore)',
    'color' => '#1362b7'
  ],
  [
    'classes' => 'rspp',
    'position' => ['top'=>730,'left'=>100],
    'title' => 'Inizio 25 giugno',
    'content' => 'Datore di lavoro che svolge il compito di responsabile del servizio di prevenzione e protezione (RSPP)<br>(16 ore) ',
    'color' => '#aa229d'
  ],
  [
    'classes' => 'formazione-specifica-per-impiegati',
    'position' => ['top'=>475,'left'=>25],
    'title' => 'Inizio 25 giugno',
    'content' => 'Formazione specifica per impiegati<br>(8 ore)',
    'color' => '#28a9e0'
  ],
  [
    'classes' => 'rischi-chimici',
    'position' => ['top'=>190,'left'=>70],
    'title' => 'Inizio 25 giugno',
    'content' => 'Corso<br>rischi chimici<br>(8 ore)',
    'color' => '#f9a02b'
  ],
]
?>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Revisione biennale - ADR 2019 in vigore dal 1° Luglio prossimo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body with-scientist">
        <div class="row">
          <div class="col-md-8">
            <p>
              Dal 1° Luglio prossimo i trasporti di merci pericolose, ovvero tutte le merci che possono recare danno a persone ad altri oggetti o all’ambiente, effettuati  per strada, oppure per ferrovia, o per vie navigabili interne, dovranno essere effettuati rispettando le disposizioni contenute nelle edizioni 2019 di ADR, RID e ADN.
              Il 30 Giugno termina, infatti, il transitorio di sei mesi durante il quale, pur essendo già in vigore dal 1° gennaio l’edizione ADR 2019, gli operatori potevano continuare la propria attività nel rispetto delle norme precedenti definite da ADR 2017.

              Dal 1° Luglio l’edizione 2019 sarà l’unica applicabile.
            </p>
            <ul>
              <li>Queste alcune delle principali novità, ma si rimanda alla pagina di aggiornamento normativo per i dettagli:</li>
              <li>l’estensione dell’obbligo di nomina del consulente per la sicurezza dei trasporti;</li>
              <li>le modifiche alla gestione dell’esenzione per unità di trasporto (cap. 1.1.3.6 dell’ADR);</li>
              <li>la nuova gestione dei macchinari o dispositivi contenenti merci pericolose;</li>
              <li>il nuovo metodo per la classificazione delle materie corrosive (classe 8).</li>
            </ul>
          </div>

          <div class="col-md-4 fancy-col" style="background-image:url('<?=$tp?>/css/images/modal-bg.png')">
            <img style="z-index:9999" class="scientist" src="<?=$tp?>/css/images/scientist.png" alt="">
          </div>
        </div>


      </div>
      <!--  <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Save changes</button>
    </div> -->
  </div>
</div>
</div>

<div class="container-fluid pl-0 pr-0">
  <div class="row">
    <div class="col-md-12">
      <div class="front-page-hero" style="background-image:url('<?=$tp?>/css/images/bg-front-page.jpg')"></div>
    </div>
  </div>
</div>
<div class="container" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
  <div class="row front-page-points">
    <div class="col-md-4 text-center">
      <div class="front-page-point" style="background-image:url('<?=$tp?>/css/images/front-page-1.png')">
        <img src="<?=$tp?>/css/images/front-page-1-icon.png" alt="">
        <p>Laboratori</p>
        <a href="/laboratori/">Scopri di più<br><i class="fas fa-2x fa-caret-down"></i></a>
      </div>
    </div>
    <div class="col-md-4 text-center">
      <div class="front-page-point" style="background-image:url('<?=$tp?>/css/images/front-page-2.png')">
        <img src="<?=$tp?>/css/images/front-page-2-icon.png" alt="">
        <p>Consulenza</p>
        <a href="/consulenza/">Scopri di più<br><i class="fas fa-2x fa-caret-down"></i></a>
      </div>
    </div>
    <div class="col-md-4 text-center">
      <div class="front-page-point" style="background-image:url('<?=$tp?>/css/images/front-page-3.png')">
        <img src="<?=$tp?>/css/images/front-page-3-icon.png" alt="">
        <p>Formazione</p>
        <a href="/formazione/">Scopri di più<br><i class="fas fa-2x fa-caret-down"></i></a>
      </div>
    </div>
  </div>
</div>

  <!--
  <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
  <span>News Normative</span>
</div>
<div class="row">
<div class="col-md-10 offset-md-1">
<div class="row">
<?php for($i = 0 ; $i <= 2 ; $i++){?>
<div class="col-md-4">
<div class="front-page-news">
<span>Aggiornamenti al 06/06</span>
<p>Linee guida per la valutazione di impatto sanitario VIS applicabili per procedimenti presentati dal 30 luglio prossimo</p>
<a href="javascript:" data-toggle="modal" data-target="#exampleModal">Leggi tutto <i class="fas fa-caret-right"></i> </a>
</div>
</div>
<?php }?>
</div>
</div>
</div>
-->
<div class="classes-program" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
  <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg-revert.png')">
    <span>Corsi In Programma</span>
  </div>
  <div class="d-none d-md-block">


    <div class="classes-program-content " style="background-image:url('<?=$tp?>/css/images/classes-program.png')">
      <?php foreach($classes as $class){?>
        <div class="classes-program-item <?=$class['classes']?>" style="top:<?=$class['position']['top']?>px; left:<?=$class['position']['left']?>px;">
          <!--<span style="color:<?=$class['color']?>"><?=$class['title']?></span>-->
          <p><?=$class['content']?></p>
        </div>
      <?php } ?>
    </div>
  </div>
  <div class="d-md-none classes-program-content-mobile">

    <?php foreach($classes as $index => $class){
      if($index % 2 == 0){ ?>
        <div class="row  mb-4">
          <div class="col-6 text-right">
            <img src="<?=$tp?>/css/images/round-<?=$index?>.png" alt="">
          </div>
          <div class="col-6 text-left">
            <p><?=$class['content']?></p>
          </div>
        </div>
      <?php }else{?>
        <div class="row mb-4">
          <div class="col-6 text-right">
            <p><?=$class['content']?></p>
          </div>
          <div class="col-6 text-left">
            <img src="<?=$tp?>/css/images/round-<?=$index?>.png" alt="">
          </div>
        </div>
      <?php }
    } ?>

  </div>
  <br>
  <a href="/wp-content/uploads/programma-corsi.pdf" target="_blank" class="all-classes-btn" style="background-image:url('<?=$tp?>/css/images/button-all-classes.png')">Scopri tutti i corsi<br>in programma</a>
  <br>
  <img style="width:180px; margin-top: 50px;" class="scientist mb-4 d-md-none" src="<?=$tp?>/css/images/scientist.png" alt="">
</div>




<?php get_footer();?>
