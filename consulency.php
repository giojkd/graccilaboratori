<?php /* Template Name: Consulency */ ?>
<?php $tp = get_template_directory_uri(); ?>
<?php $icons = [
  [
    'index' => 'ambiente',
    'url' => '/settore-ambientale/'
  ],
  [
    'index' => 'igiene-lavoro',
    'url' => '/settore-igiene-del-lavoro/'
  ],
  [
    'index' => 'alimentare',
    'url' => '/settore-alimentare/'
  ],
  [
    'index' => 'moca',
    'url' => '/settore-materiali-destinati-al-contatto-alimentare-moca/'
  ],
  [
    'index' => 'animale',
    'url' => '/settore-sottoprodotti-di-origine-animale/'
  ]
]; ?>

<?php $bgs = ['00b1f5','e246a4','ff2b69','ff5605','8dd400']; ?>
<?php
$post   = get_post( 9 );
$pageContent =  apply_filters( 'the_content', '<?xml encoding="utf-8" ?>'.$post->post_content );
$dom = new DOMDocument();
$dom->loadHTML($pageContent);
$h2 =  $dom->getElementsByTagName('h2')[0];
$h3s =  $dom->getElementsByTagName('h3');

?>
<?php get_header();?>

<div class="container-fluid  pl-0 pr-0">
  <div class="row">
    <div class="col-md-12">
      <?php the_post_thumbnail(); ?>
    </div>
  </div>
</div>
<div class="container-fluid" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
        <span>Consulenza</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-10 offset-md-1 text-center">
      <div class="d-none d-md-block">


        <div class="consulency " style="background-image:url('<?=$tp?>/css/images/consulency-bg.png')">
          <?php foreach($icons as $icon){
            ?>
            <div class="icon">
              <a href="<?=$icon['url']?>">
                <img src="<?=$tp?>/css/images/icon-<?=$icon['index']?>.png" alt="">
              </a>
            </div>
            <?php
          }?>

          <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
          the_content();
        endwhile; else: ?>
        <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
      <div class="scientist" style="background-image:url('<?=$tp?>/css/images/scientist.png')"></div>
    </div>
  </div>
  <div class="consulency-mobile d-md-none">
    <?php foreach ($h3s as $key => $h3): ?>
      <div class="text-center">
        <div class="consulency-box" style="background-color:#<?=$bgs[$key]?>">
          <a  href="<?=$icons[$key]['url']?>">
            <img src="<?=$tp?>/css/images/icon-<?=$icons[$key]['index']?>.png" alt="">
            <h3><?=$h3->nodeValue?></h3>
          </a>
        </div>
      </div>
      <div class="dashed-separator"></div>
    <?php endforeach; ?>
    <div class="text-center mb-4">
        <img src="<?=$tp?>/css/images/scientist.png" alt="">
    </div>
  </div>
  <?php
  if(count($files)>0){
    foreach($files as $file){
      ?>
      <a download target="_blank" href="<?=$file['url']?>" class="file-box">
        <img src="<?=$tp?>/css/images/icon-pdf.png" alt=""> <?=$file['title']?>
      </a>
      <?php
    }
  }?>
</div>
</div>
</div>
<?php get_footer();?>
