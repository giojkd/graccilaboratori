$(function(){
  var flick = $('.main-carousel').flickity({
    // options
    cellAlign: 'left',
    wrapAround: true,
    adaptiveHeight: true,
    percentPosition: true,
    initialIndex: 0,
    pageDots: false,
    prevNextButtons: false
  });

  flick.on('dragStart.flickity',function(){
    $('.stack-expander').animate({
      opacity: 0,
    }, 700, function() {});
  });

  flick.on('settle.flickity', function(event,index) {

    var settledAt = parseInt(index);
    if(settledAt < ($('.carousel-cell').length -1 )){
      settledAt++;
    }else{
      settledAt = 0;
    }
    console.log(index+ ' -> '+settledAt);
    var settledSlide = $('.carousel-cell').eq(settledAt);
    settledSlide.find('.stack-expander').animate({
      opacity: 1,
    }, 700, function() {});

    var cat = settledSlide.data('cat');

    createBigGallery(cat);
  });

  $('.carousel-cell').eq(1).find('.stack-expander').animate({
    opacity: 1,
  }, 700, function() {});

  $('.rotate-180').click(function() {
    var degrees = 180;


    $(this).css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
    '-moz-transform' : 'rotate('+ degrees +'deg)',
    '-ms-transform' : 'rotate('+ degrees +'deg)',
    'transform' : 'rotate('+ degrees +'deg)'});
  });

  var firstCat = Object.keys(slides)[1];

  createBigGallery(firstCat);

})

function createBigGallery(cat){
  var sl = slides[cat].photos;
  var pathPrefix = slides[cat].path_prefix;
  $('.fullpage-gallery ul').html('');
  for (var i in sl){
    $('.fullpage-gallery ul').append($('<li><img src="'+pathPrefix+'/'+sl[i]+'"></li>'))
  }
}

function hideBigGallery(){
  var degrees = 0 ;
  $('.stack-expander').css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
  '-moz-transform' : 'rotate('+ degrees +'deg)',
  '-ms-transform' : 'rotate('+ degrees +'deg)',
  'transform' : 'rotate('+ degrees +'deg)'});

  $('.fullpage-gallery').removeClass('d-none').animate({
    'opacity': 0,
    'top' : '1000px'
  }, 500, function() {
    $('.fullpage-gallery').addClass('d-none');
  });
}

function showBigGallery(){
  $('.fullpage-gallery').removeClass('d-none').animate({
    'opacity': 1,
    'top':'0px',
    backgroundColor:'#000'
  }, 500, function() {

  });
}

function selectCell(cat){
  alert(cat);
}
