<?php /* Template Name: Laboratories */ ?>
<?php $tp = get_template_directory_uri(); ?>
<?php get_header();?>
<?php

$files = [];
for($i = 1; $i <= 5; $i++){
  $files[] = array_filter((array)get_field('file_'.$i));
}
$files = array_filter($files);

$post   = get_post( 7 );

$pageContent =  apply_filters( 'the_content', '<?xml encoding="utf-8" ?>'.$post->post_content );

$dom = new DOMDocument();
$dom->loadHTML($pageContent);
$ps =  $dom->getElementsByTagName('p');
$h3s = $dom->getElementsByTagName('h2');

$labsUrlsFix = ['chimico','fisico','amianto','microbiologico'];

?>
<style media="screen">
.file-box{
  border: 1px solid grey;
}

.file-box:hover{
  color:grey;
}
</style>

<div class="container-fluid  pl-0 pr-0">
  <div class="row">
    <div class="col-md-12">
      <?php the_post_thumbnail(); ?>
    </div>
  </div>
</div>

<div class="container-fluid" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
        <span>Laboratori di Analisi</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 text-center d-none d-md-block">
      <div class="laboratories-tabs" style="background-image:url('<?=$tp?>/css/images/laboratories-tabs-bg.png')">


        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
        the_content();
      endwhile; else: ?>
      <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
    <div class="scientist" style="background-image:url('<?=$tp?>/css/images/scientist.png')"></div>
    <?php
    if(count($files)>0){
      foreach($files as $index => $file){
        ?>
        <a id="lab_file_<?=$index?>"  target="_blank" href="<?=$file['url']?>" class="file-box">
          <img src="<?=$tp?>/css/images/icon-pdf.png" alt=""> <?=$file['title']?>
        </a>
        <?php
      }
    }?>
  </div>

</div>
<div class="d-md-none ">

<?php foreach($h3s as $index => $h3){?>
  <div class="laboratories-tab-mobile" style="background-image:url('<?=$tp?>/css/images/lab-box-bg-<?=$index?>.png')">
    <h2 >  <a href="/<?=$labsUrlsFix[$index]?>">  <?=$h3->nodeValue?></a></h2>
    <p><a href="/<?=$labsUrlsFix[$index]?>"><?=str_replace( ']]>', ']]&gt;',$ps[$index]->nodeValue)?></a></p>
  </div>
  <?php }?>
<div class="text-center">


  <?php
  if(count($files)>0){
    foreach($files as $index => $file){
      ?>
      <a id="mobile_lab_file_<?=$index?>"  target="_blank" href="<?=$file['url']?>" class="file-box">
        <img src="<?=$tp?>/css/images/icon-pdf.png" alt=""> <?=$file['title']?>
      </a>
      <?php
    }
  }?>
  <img style="z-index:9999" class="scientist mt-4 mb-4" src="<?=$tp?>/css/images/scientist.png" alt="">
</div>
</div>
</div>
</div>
<?php get_footer();?>
