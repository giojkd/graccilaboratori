<?php /* Template Name: AboutUs */ ?>
<?php

$post   = get_post( 2 );

$pageContent =  apply_filters( 'the_content', '<?xml encoding="utf-8" ?>'.$post->post_content );
$dom = new DOMDocument();
$dom->loadHTML($pageContent);
$ps =  $dom->getElementsByTagName('p');
$h3s = $dom->getElementsByTagName('h3');
$h2 = $dom->getElementsByTagName('h2')[0];

 ?>
<?php $tp = get_template_directory_uri(); ?>
<?php get_header();?>
<div class="container-fluid  pl-0 pr-0" >
  <div class="row">
    <div class="col-md-12">
      <img class="page-cover" src="<?=$tp?>/css/images/about-us-page-cover.png" alt="">
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 text-center" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')" >
      <div class="d-none d-md-block">
        <div id="about-us-page" style="background-image:url('<?=$tp?>/css/images/about-us-bg.png')">
          <h1><?php the_title(); ?></h1>

          <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
          the_content();
        endwhile; else: ?>
        <p>Sorry, no posts matched your criteria.</p>
      <?php endif; ?>
    </div>
  </div>
  <div class="d-md-none">
    <div id="about-us-page-mobile" class="text-center">
      <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg-revert.png')">
        <span><?php the_title(); ?></span>
        </div>
        <h2 style="background-image:url('<?=$tp?>/css/images/about-us-mobile-title-bg.png')"><?=$h2->nodeValue?></h2>
        <div class="dashed-separator"></div>
      <?php foreach($h3s as $index => $h3){?>

          <h3 style="background-image:url('<?=$tp?>/css/images/round-about-us-<?=$index?>.png')"><?=$h3->nodeValue?></h3>
          <div class="dashed-separator"></div>
          <p><?=$ps[$index]->nodeValue?></p>
          <div class="dashed-separator"></div>
      <?php }?>
</div>
</div>

</div>
</div>
</div>

<?php get_footer();?>
