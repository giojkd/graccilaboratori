
<?php $footer_menu_items = wp_get_nav_menu_items('footer-menu'); ?>
<?php $second_footer_menu_items = wp_get_nav_menu_items('second-footer-menu'); ?>
<?php wp_footer();?>
<?php $tp = get_template_directory_uri();?>
<div class="container awards">
  <div class="row">
    <div class="col-md-12">
      <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
        <span>Riconoscimenti</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 text-center mb-4"><a href="#" class="text-dark border border-dark rounded-xxl py-2 px-4 d-inline-block text-uppercase font-weight-bold font-size-13px" style="height:120px;">Agenzia Formativa<br>Accreditata dalla<br>Regione Toscana al<br>n° OF0039 con decreto<br>n. 18791 del 27/11/2018</a></div>
    <div class="col-md-4 text-center mb-4"><a target="_blank" href="/wp-content/uploads/2019/07/CERTIFICATO-ACCREDIA-Iso-17025-_anno-2019-copia.pdf" class="text-dark border border-dark rounded-xxl px-4 d-inline-block text-uppercase font-weight-bold font-size-13px" style="height:120px; padding-top: 30px;">Certificato di<br>Accreditamento UNI CEI<br>EN ISO/IEC 17025:2005</a></div>
    <div class="col-md-4 text-center mb-4"><a target="_blank" href="/wp-content/uploads/2019/07/CERTIFICATO-ISO-9001-GRACCI-LABORATORI-SRL_anno-2019.pdf" class="text-dark border border-dark rounded-xxl px-4 d-inline-block text-uppercase font-weight-bold font-size-13px" style="height:120px; padding-top: 20px;">Laboratori e Agenzia<br>Formativa con SGQ<br>certificato<br>UNI EN ISO 9001:2015</a></div>
  </div>
</div>
<div id="footer">

  <div class="container bg-light rounded-xl pb-4">
    <h1 class="text-center">Richiedi informazioni</h1>
    <form class="" action="index.html" method="post">
      <div class="row">
        <div class="col-md-4">
          <input type="text" class="rounded-xl" name="" value="" placeholder="Nome">
          <input type="text" class="rounded-xl" name="" value="" placeholder="Cognome">
          <input type="text" class="rounded-xl" name="" value="" placeholder="Azienda">
          <input type="text" class="rounded-xl" name="" value="" placeholder="Codice promo">
          <input type="text" class="rounded-xl" name="" value="" placeholder="Email">
        </div>
        <div class="col-md-4">
          <textarea name="name" rows="8"  placeholder="Messaggio"></textarea>
        </div>
        <div class="col-md-4">
          <label>Questo modulo di contatto raccoglie il tuo nome, l'e-mail e il tuo contenuto.<br>
Per maggiori informazioni consulta la nostra pagina di <a href="/privacy/">Privacy Policy</a><br></label>
<label><input type="checkbox" required name="" value=""> Ho letto l'informativa privacy e accetto la memorizzazione dei miei dati</label><br>
<label><input type="checkbox" name="" value=""> Acconsento ai sensi del Regolamento Europeo 2016/679 ad essere informato su promozioni e novità tramite email/SMS, a ricevere la vostra newsletter e i questionari per richiedere una prima consulenza gratuita. (Opzionale)</label><br>

          <div class="g-recaptcha" data-sitekey="6Lf5EKsUAAAAAKUUx1J_kNRjxpx-rAbRJxDN_92W"></div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary mt-4 rounded-xl">Invia</button>
          </div>
        </div>
      </div>
    </div>
    <div class="container bg-white mt-4">
      <div class="row">
        <div class="col-md-4">
          <div class="footer-box border row no-gutters">
            <div class="col-3 border-right text-center">
              <span class="gracci-blue d-block p-4">
                <i class="fas fa-phone fa-2x"></i>
              </span>
            </div>
            <div class="col-9 pt-3 pl-3">
              <span>Hai qualche domanda? Chiamaci!</span>
              <p>Tel 0571 591184</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="footer-box border row no-gutters">
            <div class="col-3 border-right text-center">
              <span class="gracci-blue d-block p-4">
                <i class="far fa-envelope fa-2x"></i>
              </span>
            </div>
            <div class="col-9 pt-3 pl-3">
              <span>Hai bisogno di supporto?</span>
              <p>info@graccilaboratori.it</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="footer-box border row no-gutters">
            <div class="col-3 border-right text-center">
              <span class="gracci-blue d-block p-4">
                <i class="far fa-clock fa-2x"></i>
              </span>
            </div>
            <div class="col-9 pt-3 pl-3">
              <span>Siamo aperti dal </span>
              <p>Lun-Ven<br>8:30-13:00 | 14:30-18:00</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container mt-4 footer-bottom">
      <div class="row">
        <div class="col-md-3">
          <h2>Servizi</h2>
          <div class="row">
            <?php foreach($footer_menu_items as $footer_menu_item){
              ?>
              <div class="col-md-6">
                <a href="<?=$footer_menu_item->url?>"> <i class="fa fa-angle-right"></i> <?=$footer_menu_item->title?></a>
              </div>
              <?php
            }?>
          </div>
        </div>
        <div class="col-md-3">
          <h2>Contattaci</h2>
          <a href="tel:+390571591194"> (+39) 0571 591194</a> - <a href="tel:+390571591184"> (+39) 0571 591184</a>
        </div>
        <div class="col-md-3">
          <h2>Pec</h2>
          <a href="mailto:patrizio.gracci@pec.chimici.it">patrizio.gracci@pec.chimici.it</a>
        </div>
        <div class="col-md-3">
                  <h2>Policies</h2>
          <div class="row">
            <?php foreach($second_footer_menu_items as $footer_menu_item){
              ?>
              <div class="col-md-6">
                <a href="<?=$footer_menu_item->url?>"> <i class="fa fa-angle-right"></i> <?=$footer_menu_item->title?></a>
              </div>
              <?php
            }?>
          </div>
        </div>
      </div>
      <div style="height:100px;"></div>
      <div class="row">
        <div class="col-md-12 text-center">
          <img src="<?php echo get_template_directory_uri()?>/css/images/logo-footer.png" alt="">
        </div>
      </div>
      <div style="height:100px;"></div>
    </div>
  </form>
</div>

</div>
</body>
</html>
