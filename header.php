<!DOCTYPE html>
<html lang="it" dir="ltr">
<head>
  <meta charset="utf-8">
  <title><?php echo wp_title(); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:400,500,700,800,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="69ae6c58-7e4e-45d3-ac74-1f94e979ea0c" type="text/javascript" async></script>
  <script id="CookieDeclaration" src="https://consent.cookiebot.com/69ae6c58-7e4e-45d3-ac74-1f94e979ea0c/cd.js" type="text/javascript" async></script>
  <?php wp_head();?>

  <link rel="apple-touch-icon" sizes="57x57" href="<?=get_template_directory_uri()?>/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?=get_template_directory_uri()?>/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=get_template_directory_uri()?>/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?=get_template_directory_uri()?>/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=get_template_directory_uri()?>/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?=get_template_directory_uri()?>/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?=get_template_directory_uri()?>/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?=get_template_directory_uri()?>/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri()?>/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?=get_template_directory_uri()?>/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri()?>/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?=get_template_directory_uri()?>/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri()?>/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?=get_template_directory_uri()?>/favicons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?=get_template_directory_uri()?>/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
</head>
<body <?php body_class();?>>



  <div class="fixed-top bg-white navbar-wrapper">
    <div class="pre-navbar d-none d-md-block">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="d-inline-block">
              <ul class="list list-inline d-inline">
                <li class="list-inline-item"> <a href="tel:+390571591194"> <i class="fa fa-phone"></i> Infoline:  (+39) 0571 591194</a> - <a href="tel:+390571591184"> (+39) 0571 591184</a></li>
                <li class="list-inline-item">|</li>
                <li class="list-inline-item"> <a href="mailto:info@graccilaboratori.it"> <i class="far fa-envelope"></i> info@graccilaboratori.it</a> </li>
                <li class="list-inline-item">|</li>
                <li class="list-inline-item"> <a href="https://www.google.com/maps/place/Via+Volontari+della+Liberta',+18,+50053+Empoli+FI/@43.7017269,10.8997929,17z/data=!3m1!4b1!4m5!3m4!1s0x132a68c5f364adfb:0xa21331d0eec849e0!8m2!3d43.701723!4d10.9019816" target="_blank"> <i class="fas fa-map-marker-alt"></i> Via Volontari Della Libertà 29 - Empoli</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">

      <nav class="navbar navbar-expand-lg">
        <a href="/">
          <img src="<?php echo get_template_directory_uri()?>/css/images/logo-small.png" height="50px;" class="d-inline-block align-top">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-main-menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"> <img src="<?php echo get_template_directory_uri()?>/css/images/navbar-toggler-icon.png" alt=""> </span>
        </button>
        <?php
        wp_nav_menu([
          'theme_location'=>'top-menu',
          'menu_class' => 'navbar-nav nav-fill w-100 align-items-star collapse navbar-collapse',
          'walker' => new wp_bootstrap_navwalker(),
          'container' => true,
          'depth' => 2,
        ])
        ?>
      </nav>
    </div>
  </div>
