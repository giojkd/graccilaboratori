<?php /* Template Name: Training */ ?>
<?php $file = get_field('file_1');?>
<?php $tp = get_template_directory_uri(); ?>
<?php get_header();?>
<div class="container-fluid  pl-0 pr-0">
  <div class="row">
    <div class="col-md-12">
      <?php the_post_thumbnail(); ?>
    </div>
  </div>
</div>
<div class="d-none d-md-block">


  <div class="container-fluid" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg')">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png')">
          <span>Formazione</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1 text-center">
        <div class="training " style="background-image:url('<?=$tp?>/css/images/training-bg.png')">

          <h2><span>01</span><span>Corsi in materia</span> ambientale</h2>
          <h2><span>02</span><span>Corsi sicurezza</span> alimentare</h2>
          <h2><span>03</span><span>Salute e sicurezza</span> nei luoghi di lavoro</h2>

          <div class="row">
            <div class="col-md-12 text-left">
              <a style="background-image:url('<?=$tp?>/css/images/button-all-classes.png')"  class="all-classes-btn" href="<?=$file['url']?>"><img class="float-left" src="<?=$tp?>/css/images/icon-pdf.png" alt=""><span>Scopri tutti i corsi<br>in programma</span> </a>
            </div>
          </div>
        </div>

        <?php
        if(count($files)>0){
          foreach($files as $file){
            ?>
            <a download target="_blank" href="<?=$file['url']?>" class="file-box">
              <img src="<?=$tp?>/css/images/icon-pdf.png" alt=""> <?=$file['title']?>
            </a>
            <?php
          }
        }?>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row post-training-row">
      <div class="col-md-3 offset-md-2">
        <div class="training-box" style="background-image:url('<?=$tp?>/css/images/training-info-box.png'); padding-top:60px;">
          Da anni<br>GRACCI LABORATORI, si occupa della formazione delle figure coinvolte nelle attività di prevenzione della salute e sicurezza nei luoghi di lavoro e degli addetti che operano nel settore alimentare.
        </div>
      </div>
      <div class="col-md-2">
        <img style="z-index:9999" class="scientist" src="<?=$tp?>/css/images/scientist.png" alt="">
      </div>
      <div class="col-md-3">
        <div class="training-box" style="background-image:url('<?=$tp?>/css/images/training-info-box.png')">
          ll nostro personale è a disposizione per trasformare le esigenze dei clienti in offerte formative rivolte alle aziende o ai privati cittadini, che si conformino a specifiche realtà aziendali o a percorsi di crescita professionale individuali.
        </div>
      </div>
    </div>
  </div>
</div>
<div class="d-md-none" style="background-image:url('<?=$tp?>/css/images/classes-program-bg.jpg'); padding-top:40px;">
  <div class="section-title" style="background-image:url('<?=$tp?>/css/images/section-title-bg.png'); margin-top: 0px" >
    <span>Formazione</span>
  </div>
  <div class="mobile-training text-center" style="background-image:url('<?=$tp?>/css/images/training-mobile-bg.png')">
    <h2><span>01</span><span>Corsi in materia</span> ambientale</h2>
    <h2><span>02</span><span>Corsi sicurezza</span> alimentare</h2>
    <h2><span>03</span><span>Salute e sicurezza</span> nei luoghi di lavoro</h2>
  </div>
  <div class="text-center">
      <a href="/wp-content/uploads/programma-corsi.pdf" target="_blank" class="all-classes-btn" style="background-image:url('<?=$tp?>/css/images/button-all-classes.png')">Scopri tutti i corsi<br>in programma</a>
  </div>
  <div class="text-center">
    <div class="mobile-training-info">
      <p>
        Da anni GRACCI LABORATORI, si occupa della formazione delle figure coinvolte nelle attività di prevenzione della salute e sicurezza nei luoghi di lavoro e degli addetti che operano nel settore alimentare.<br><br>
        ll nostro personale è a disposizione per trasformare le esigenze dei clienti in offerte formative rivolte alle aziende o ai privati cittadini, che si conformino a specifiche realtà aziendali o a percorsi di crescita professionale individuali.
      </p>
    </div>
  </div>



  <div class="mobile-training-scientist">
      <img style="z-index:9999" class="scientist" src="<?=$tp?>/css/images/scientist.png" alt="">
  </div>


</div>
<?php get_footer();?>
